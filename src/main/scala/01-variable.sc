46

//cast to String
46.toString()

//return Boolean
46 == 47
46 != 47

//valeure absolue
46.abs

46.toHexString

46.isValidInt

//var mutable modifiable
var mutable = 21
mutable = mutable + 1

//immuable
val immutable = 21
//impossible car immuable
//immutable = immutable + 1

val immutable2 = immutable + 1

