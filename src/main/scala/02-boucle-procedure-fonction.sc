//boucle while
var i = 0
while(i < 10){
  println(i)
  i += 1
}

//boucle for
for(i <- 0 to 9)
  println(i)

//fonctions
def max(x:Int, y:Int) = if (x>y) x else  y
max(6,8)
max(16,8)

def opp(x:Int) = -x
opp(8)
opp(-8)

//fonctions anonymes
val liste = List("bleu","rouge","vert","blanc")

liste.filter(s => s.startsWith("b"))

liste.count(s => s.startsWith("b"))

val cnt = liste.count(s => s.startsWith("b"))

//procédures
//pas de retour
def sayHello(aQui: String):Unit=
  println(s"Hello $aQui")

sayHello("John")
