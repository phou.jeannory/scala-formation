//définir fonction
//return boolean
def divs(m: Int)(n: Int) = (n % m == 0)

//on se renseigne que le 1er paramètre soit le m, le n a définir
val param1 = divs(2)_

//maintenant on renseigne n
param1(4)

//on peut re-renseigner n
param1(9)