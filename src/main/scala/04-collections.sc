//7:41
//start 20:23
//tableau est mutable
val a = new Array[String](2)

a(0) = "Java"

a(1) = "Rocks"

//afficher
a

println(a)

for(i <- 0 to 1) println(a(i))

//reaffectation
a(1) = "Scala"

for(i <- 0 to 1) println(a(i))

//List est immuable
val list1 = List(1,2,3)

val list2 = List("Thomas","Young",56)

val list01 = 1 :: 2 :: 3 :: Nil

//fonction anonyme
list01.filter(_>1)

//fonction anonyme
list01.filter(x => x > 1)

var list001 = 1 :: 2 :: 3 :: Nil

//ajouter element à une nouvelle liste
val list002 = list001 ::: 4 :: Nil

//ajouter une liste à une liste
val list003 = 0 :: list002

val list004 = list003 ::: 5 :: Nil


