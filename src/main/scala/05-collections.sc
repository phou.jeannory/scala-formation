//sets est immutable
var set1 = Set(1,2,3,4)

val set2 = Set(4,5,6,7)

val set3 = Set(1,2,3,4,4)

//minimum
set1.min

//max
set1.max

//intersection
set1.intersect(set2)

//union
set1.union(set2)

//difference
set1.diff(set2)

//on peut rendre le set mutable
var mutableset = scala.collection.mutable.Set(1,2,3,4)

//ajouter éléments
mutableset += 5

//tuple est immuable
val tuple1 = ("Thomas","Young",56)

//afficher
println(tuple1)

//affecter valeure aux variables
val(fname, lname, age) = tuple1

//afficher la valeure
fname

//equivalent
tuple1._1

//Maps est immutable
val map1 = Map("fname" -> "Thomas","lname" -> "Young", "age" -> 56)

//obtenir les keys
map1.keys

//obtenir les valeures
map1.values

var map2 = scala.collection.mutable.Map("fname" -> "Thomas","lname" -> "Young", "age" -> 56)

map2.keys

//ajouter une key valeure
map2 += ("city" -> "London")


