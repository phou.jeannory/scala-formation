def matchTest(x:Int):String = x match {
  case 0 => "Zero"
  case 1 => "One"
  case 2 => "Two"
  case _=> "Many"
}
matchTest(1)
matchTest(2)
matchTest(9)
matchTest(0)