class Personne(val name:String, var age:Int){}

var p1 = new Personne("Stephane", 32)

var p2 = new Personne("Sophie", 15)

p1.name

p2.name

class Point(var x:Int, var y:Int){}

val point1 = new Point(2,3)

point1.x

point1.y

class PointV2(var x:Int=0, var y:Int=0)

val po1 = new PointV2()

po1.x

po1.y

val po2 = new PointV2(5)

po2.x

po2.y

val po3 = new PointV2(y=2)

po3.x

po3.y