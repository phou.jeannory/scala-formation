case class Message(expediteur: String, destinataire: String, message:String)

val message1 = Message("thomas","durand","hello world")

println(message1.expediteur)

println(message1.message)

case class Message2(var expediteur: String, destinataire: String, message:String)

val message2 = Message2("thomas","durand","hello world")

println(message2.expediteur)

println(message2.message)

message2.expediteur = "other"

println(message2.expediteur)
